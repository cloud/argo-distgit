%global with_unit_test 0
%if 0%{?fedora}
%global with_devel   0
%global with_bundled 0
%global with_debug   0
%else
%global with_devel   0
%global with_bundled 0
%global with_debug   0
%endif

%if 0%{?with_debug}
%global _dwz_low_mem_die_limit 0
%else
%global debug_package   %{nil}
%endif


%global provider        github
%global provider_tld    com
%global project         argoproj
%global repo            argo
# https://github.com/helm/helm
%global provider_prefix %{provider}.%{provider_tld}/%{project}/%{repo}
%global import_path     argoproj/argo
%global commit          88fcc70dcf6e60697e6716edc7464a403c49b27e
%global shortcommit     %(c=%{commit}; echo ${c:0:7})

%global argo_version            2.3.0
%global argo_git_version        v%{argo_version}

##############################################

Name:           argo
Version:        %{shortcommit}
Release:        2%{?dist}
Summary:        Argo Workflows: Get stuff done with Kubernetes.
License:        ASL 2.0
URL:            https://%{provider_prefix}
Source0:        https://%{provider_prefix}/archive/%{commit}/%{repo}-%{shortcommit}.tar.gz


BuildRequires: gcc
BuildRequires: golang

%description
%{summary}

%prep
%setup -n %{name}-%{shortcommit}
# Move all the code under src/k8s.io/kubernetes directory
###############
mkdir -p src/github.com/argoproj
mv argo src/github.com/argoproj

%build
export GOPATH=$(pwd)
export GIT_COMMIT=%{commit}
export GIT_SHA=%{shortcommit}
export GIT_TAG=%{argo_git_version}
export GIT_DIRTY=clean
export VERSION=%{argo_git_version}
export BUILDATE=$(date -u +'%Y-%m-%dT%H:%M:%SZ')
export PACKAGE=github.com/argoproj/argo
cd src/github.com/argoproj/argo/cmd/argo
go build -ldflags "-X ${PACKAGE}.version=${VERSION} -X ${PACKAGE}.gitCommit=${GIT_COMMIT} -X ${PACKAGE}.gitTreeState=${GIT_DIRTY} -X ${PACKAGE}.buildDate=${BUILDATE} -X ${PACKAGE}.gitTag=${GIT_TAG} -extldflags '-static'" .
./argo version

%install
install -D -p -m 0755 src/github.com/argoproj/argo/cmd/argo/argo %{buildroot}%{_bindir}/argo
# install completion
install -d -m 0755 %{buildroot}%{_datadir}/bash-completion/completions/
%{buildroot}%{_bindir}/argo completion bash > %{buildroot}%{_datadir}/bash-completion/completions/argo

%files
%{_bindir}/argo
%{_datadir}/bash-completion/completions/argo

%changelog
* Tue Jul 09 2019 Spyros Trigazis <spyridon.trigazis@cern.ch> - 2.3.0-2.el7.cern
- Fix initial release where deps were not added.

* Fri Jul 05 2019 Spyros Trigazis <spyridon.trigazis@cern.ch> - 2.3.0-1.el7.cern
- Initial package

#!/bin/bash -x

commit=$1

cwd=$(pwd)
mkdir -p "${cwd}/argo"
export GOPATH=/tmp/gopath
mkdir -p $GOPATH/src/github.com/argoproj
cd "$GOPATH/src/github.com/argoproj"
git clone https://github.com/argoproj/argo.git
cd argo
git checkout "${commit}"
dep ensure -vendor-only
cp -r ./* "${cwd}/argo/"
cd "${cwd}"
